﻿using Beepr.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;


namespace WebApi.Controllers
{
    [RoutePrefix("api/holidayCalc")]
    public class HolidayCalculatorController : ApiController
    {
        private IConfigSource<PublicHolidayDefinitions> _configSource;

        public HolidayCalculatorController(IConfigSource<PublicHolidayDefinitions> configSource)
        {
            _configSource = configSource;
        }


        // GET: HolidayCalculator
        [Route("distance")]
        [HttpGet]
        [HttpPost]
        [ResponseType(typeof(int))]
        public async Task<IHttpActionResult> Distance(string calendarName, string firstDate, string secondDate)
        {
            try
            {
                var result = 0;

                await Task.Run(() =>
                {

                    var holidayCalculator =
                        new PublicHolidayCalculator(_configSource, calendarName, Log.Logger);

                    result = holidayCalculator.ComputeWorkDaysBetween(DateTime.Parse(firstDate)
                                                    , DateTime.Parse(secondDate));

                });

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }



    }
}