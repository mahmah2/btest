﻿using Beepr.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;
using WebApi.Controllers;

namespace WebApi
{
    public class CustomControllerActivator : IHttpControllerActivator
    {
        public IHttpController Create(
            HttpRequestMessage request,
            HttpControllerDescriptor controllerDescriptor,
            Type controllerType)
        {
            if (controllerDescriptor.ControllerType.FullName == typeof(HolidayCalculatorController).FullName)
            {
                return new HolidayCalculatorController(new FileSystemHolidayDefinitionLoader());
            }

            return null;
        }
    }
}