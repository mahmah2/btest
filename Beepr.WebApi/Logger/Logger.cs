﻿using Beepr.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi
{
    public static class Log
    {
        public static ILogger Logger { get; set; } = new FileSystemLogger();
    }
}