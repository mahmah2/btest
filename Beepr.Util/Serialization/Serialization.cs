﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Beepr.Util
{
    public static class Serialization
    {
        public static string Serialize<T>(this T self)
        {
            MemoryStream ms = new MemoryStream();
            XmlSerializer xs = new XmlSerializer(typeof(T));
            xs.Serialize(ms, self);
            return Encoding.ASCII.GetString(ms.ToArray());
        }

        public static byte[] SerializeToBytes<T>(this T self)
        {
            return Encoding.ASCII.GetBytes(Serialize<T>(self));
        }

        public static T Deserialize<T>(string input)
        {
            XmlSerializer xs = new XmlSerializer(typeof(T));
            MemoryStream ms = new MemoryStream(ASCIIEncoding.ASCII.GetBytes(input));
            ms.Position = 0;
            return (T)xs.Deserialize(ms);
        }

        public static T DeserializeFromBytes<T>(byte[] input)
        {
            return Deserialize<T>(Encoding.ASCII.GetString(input));
        }

    }
}
