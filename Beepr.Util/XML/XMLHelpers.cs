﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Beepr.Util.XML
{
    public class XMLHelpers
    {
        public static string GetRootNodeElementNameForType(Type serializedObjectType)
        {
            XmlRootAttribute theAttrib = Attribute.GetCustomAttribute(
                                serializedObjectType, typeof(XmlRootAttribute)) as XmlRootAttribute;

            if (theAttrib != null)
            {
                if (String.IsNullOrEmpty(theAttrib.ElementName) == false)
                {
                    return theAttrib.ElementName;
                }
                else
                {
                    return serializedObjectType.Name;
                }
            }
            else
            {
                return serializedObjectType.Name;
            }
        }

    }
}
