﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beepr.Util
{
    public static class FileHelpers
    {
        public static string ExeDirectory()
        {
            return AppDomain.CurrentDomain.BaseDirectory;
        }

        public static string NormaliseFileName(string fileName)
        {
            var adjustedFilePath = Path.Combine(ExeDirectory(), fileName);

            return adjustedFilePath;
        }

    }
}