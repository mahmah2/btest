﻿namespace Beepr.Business
{
    public interface IConfigSource<T>
    {
        void Init(string data);
        T LoadData(string configuration);
    }
}
