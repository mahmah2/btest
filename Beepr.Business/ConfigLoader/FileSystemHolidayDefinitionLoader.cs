﻿using System;
using System.IO;

namespace Beepr.Business
{
    public class FileSystemHolidayDefinitionLoader : IConfigSource<PublicHolidayDefinitions>
    {
        public void Init(string data)
        {

        }

        public PublicHolidayDefinitions LoadData(string rawXml)
        {
            var result = Beepr.Util.Serialization.Deserialize<PublicHolidayDefinitions>(rawXml);

            return result;
        }
    }
}
