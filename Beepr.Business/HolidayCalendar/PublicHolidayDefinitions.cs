﻿using Beepr.Util;
using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Beepr.Business
{
    [XmlRoot("PublicHolidayDefinitions")]
    public class PublicHolidayDefinitions : List<PublicHolidayBase> , IXmlSerializable
    {
        public List<DayOfWeek> WeekendDays = new List<DayOfWeek>();

        public PublicHolidayDefinitions()
        {
        }

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            Clear();
            WeekendDays.Clear();

            var nodeName = Beepr.Util.XML.XMLHelpers.GetRootNodeElementNameForType(this.GetType());

            if (reader.IsEmptyElement)
            {
                throw new ApplicationException($"Missing child elements: {nodeName}");
            }

            

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Element) // The node is an element.
                {
                    if (reader.Name == "WeekendDays")
                    {
                        XmlDocument doc = new XmlDocument();
                        XmlReader inner = reader.ReadSubtree();
                        XmlNode myNode = doc.ReadNode(inner);
                        
                        XmlNodeList days = myNode.SelectNodes("/Day/@name");
                        foreach (XmlNode day in days)
                        {
                            WeekendDays.Add((DayOfWeek)Enum.Parse(typeof( DayOfWeek ),  day.InnerText ));
                        }
                    }
                    else if (reader.Name == "Definitions")
                    {
                        XmlDocument doc = new XmlDocument();
                        XmlReader inner = reader.ReadSubtree();
                        XmlNode myNode = doc.ReadNode(inner);

                        XmlNodeList definitions = myNode.SelectNodes("/*");
                        foreach (XmlNode definitionNode in definitions)
                        {
                            if (definitionNode.Name == "FixedDateHoliday")
                            {
                                var definition = Util.Serialization.Deserialize<FixedDateHoliday>(definitionNode.OuterXml);

                                Add(definition);
                            }
                            else if (definitionNode.Name == "FirstWeekdayHoliday")
                            {
                                var definition = Util.Serialization.Deserialize<FirstWeekdayHoliday>(definitionNode.OuterXml);

                                Add(definition);
                            }
                            else if (definitionNode.Name == "NOccurenceHoliday")
                            {
                                var definition = Util.Serialization.Deserialize<NOccurenceHoliday>(definitionNode.OuterXml);

                                Add(definition);
                            }


                        }
                    }

                }
                else if (reader.NodeType == XmlNodeType.EndElement)
                {
                    if (reader.Name.Equals(nodeName))
                        break;
                }

            }
        }

        public void WriteXml(XmlWriter writer)
        {
        }
    }
}
