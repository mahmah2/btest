﻿using System;
using System.Xml;

namespace Beepr.Business
{
    public class NOccurenceHoliday : PublicHolidayBase
    {
        protected DayOfWeek DayOfWeek { get; set; }
        protected int Count { get; set; }

        public NOccurenceHoliday()
        {

        }

        public NOccurenceHoliday(string title, int monthNumber, int count, DayOfWeek dayOfWeek)
        {
            Title = title;
            MonthNumber = monthNumber;
            Count = count;
            DayOfWeek = dayOfWeek;
        }

        public override DateTime GetDateInYear(int year)
        {
            int occuranceCounter = 0;

            var result = new DateTime(year, MonthNumber, 1);

            var daysInMonth = DateTime.DaysInMonth(year, MonthNumber);

            for (int i = 1; i <= daysInMonth; i++)
            {
                if (result.DayOfWeek == DayOfWeek)
                    occuranceCounter++;

                if (Count == occuranceCounter)
                {
                    return result;
                }

                result = result.AddDays(1);
            }

            return result;
        }

        public override void OnReadUnknownXmlNode(XmlReader reader)
        {
            if (reader.Name == "Count")
            {
                reader.Read();

                Count = int.Parse( reader.Value);
            }
            else if (reader.Name == "DayOfTheWeek")
            {
                reader.Read();

                DayOfWeek = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), reader.Value);
            }
        }
    }
}
