﻿using System;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Beepr.Business
{
    public class PublicHolidayBase: IXmlSerializable
    {
        protected string    Title { get; set; }
        protected int       DayNumber { get; set; }
        protected int       MonthNumber { get; set; }
        protected int       DayNumberInMonth { get; set; }

        public virtual DateTime GetDateInYear(int year)
        {
            return new DateTime(year, 1, 1);
        }

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            var nodeName = Beepr.Util.XML.XMLHelpers.GetRootNodeElementNameForType(this.GetType());

            if (reader.IsEmptyElement)
            {
                throw new ApplicationException($"Missing child elements: {nodeName}");
            }

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Element) // The node is an element.
                {
                    if (reader.Name == "Title")
                    {
                        reader.Read();

                        Title = reader.Value;
                    }
                    else if (reader.Name == "MonthNumber")
                    {
                        reader.Read();

                        MonthNumber = int.Parse(reader.Value);
                    }
                    else if (reader.Name == "DayNumberInMonth")
                    {
                        reader.Read();

                        DayNumberInMonth = int.Parse(reader.Value);
                    }
                    else
                        OnReadUnknownXmlNode(reader);

                }
                else if (reader.NodeType == XmlNodeType.EndElement)
                {
                    if (reader.Name.Equals(nodeName))
                        break;
                }
            }
        }

        virtual public void OnReadUnknownXmlNode(XmlReader reader)
        {

        }

        public void WriteXml(XmlWriter writer)
        {
            throw new NotImplementedException();
        }
    }
}
