﻿using System;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Beepr.Business
{
    [XmlRoot("FixedDateHoliday")]
    public class FixedDateHoliday : PublicHolidayBase
    {
        public FixedDateHoliday()
        {
        
        }

        public FixedDateHoliday(string title, int monthNumber, int dayInMonthNumber)
        {
            Title = title;
            MonthNumber = monthNumber;
            DayNumberInMonth = dayInMonthNumber;
        }

        public override DateTime GetDateInYear(int year)
        {
            return new DateTime(year, MonthNumber, DayNumberInMonth);
        }

 
    }
}
