﻿namespace Beepr.Business
{
    public enum HolidayType
    {
        FixedDate,
        FirstWeekDay,
        CertainDayInMonth,
    }
}
