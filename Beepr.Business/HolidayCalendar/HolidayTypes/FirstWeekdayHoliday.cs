﻿using System;

namespace Beepr.Business
{
    public class FirstWeekdayHoliday : PublicHolidayBase
    {
        public FirstWeekdayHoliday()
        {
        }

        public FirstWeekdayHoliday(string title, int monthNumber, int dayInMonthNumber)
        {
            Title = title;
            MonthNumber = monthNumber;
            DayNumberInMonth = dayInMonthNumber;
        }

        public override DateTime GetDateInYear(int year)
        {
            var result = new DateTime(year, MonthNumber, DayNumberInMonth);

            if (result.DayOfWeek == DayOfWeek.Saturday)
            {
                result = result.AddDays(2);
            }
            else if (result.DayOfWeek == DayOfWeek.Sunday)
            {
                result = result.AddDays(1);
            }

            return result;
        }
    }
}
