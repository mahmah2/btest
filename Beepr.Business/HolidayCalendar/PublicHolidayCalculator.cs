﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beepr.Business
{

    public class PublicHolidayCalculator
    {
        private PublicHolidayDefinitions _definitions;
        private ILogger _logger;

        public PublicHolidayCalculator(IConfigSource<PublicHolidayDefinitions> datasource
                                        ,ILogger logger                            
                                        ,string definitionsXml)
        {
            _definitions = datasource.LoadData(definitionsXml);

            _logger = logger;
        }

        public PublicHolidayCalculator(IConfigSource<PublicHolidayDefinitions> datasource
                                       ,string calendarName
                                       ,ILogger logger)

        {
            var configText = string.Empty;
            var calendarFileName = Beepr.Util.FileHelpers.NormaliseFileName(
                Global.ApplicationFolderPrefix +  calendarName + ".XML" );

            if (File.Exists(calendarFileName))
            {
                configText = File.ReadAllText(calendarFileName);
            }
            else
            { 
                throw new Exception($"Config file doesn't exist : {calendarFileName}");
            }

            _definitions = datasource.LoadData(configText);

            _logger = logger;

            //TODO : we can create a cache at this stage
        }

        private bool IsHolliday(DateTime date)
        {
            if (_definitions.WeekendDays.Contains(date.DayOfWeek))
                return true;

            //check all holidays on this year
            foreach (var item in _definitions)
            {
                if ((item.GetDateInYear(date.Year) - date).Days == 0)
                {
                    return true;
                }
            }

            return false;
        }


        public int ComputeWorkDaysBetween(DateTime firstDate, DateTime secondDate)
        {
            int result = 0;

            if (secondDate < firstDate)
            {
                throw new ArgumentException("First date should be earlier than the second date.");
            }

            for (var currentDay = firstDate.AddDays(1); currentDay < secondDate; currentDay = currentDay.AddDays(1))
            {
                if (!IsHolliday(currentDay))
                {
                    result++;
                }
            }

            _logger.LogInfo($"firstDate={firstDate} , secondDate={secondDate}, result={result}");

            return result;
        }

    }
}
