﻿namespace Beepr.Business
{
    public class FileSystemLogger : ILogger
    {
        public void LogDebug(string msg)
        {
        }

        public void LogError(string msg)
        {
        }

        public void LogException(string msg)
        {

        }

        public void LogInfo(string msg)
        {
        }

        public void LogWarning(string msg)
        {
        }
    }
}
