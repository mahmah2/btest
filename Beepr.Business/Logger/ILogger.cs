﻿namespace Beepr.Business
{
    public interface ILogger
    {
        void LogInfo(string msg);
        void LogWarning(string msg);
        void LogError(string msg);
        void LogDebug(string msg);
        void LogException(string msg);
    }
}
