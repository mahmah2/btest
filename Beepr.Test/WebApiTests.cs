﻿using System;
using System.Threading.Tasks;
using System.Web.Http.Results;
using Beepr.Business;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebApi.Controllers;

namespace Beepr.Test
{
    [TestClass]
    public class WebApiTests
    {
        [TestMethod]
        public void AUSNSWGeneralTestsAsync()
        {
            var controller = new HolidayCalculatorController(new FileSystemHolidayDefinitionLoader());

            Business.Global.ApplicationFolderPrefix = @"Configs\";

            var result = controller.Distance("AUS-NSW", "2014-8-7", "2014-8-11");

            Assert.IsNotNull(result);

            Assert.AreEqual(1, (result.Result as OkNegotiatedContentResult<int>).Content  );

            result = controller.Distance("AUS-NSW", "2014-8-13", "2014-8-21");

            Assert.IsNotNull(result);

            Assert.AreEqual(5, (result.Result as OkNegotiatedContentResult<int>).Content);
        }

        [TestMethod]
        public  void  AUSNSWInvalidCaseAsync2()
        {
            var controller = new HolidayCalculatorController(new FileSystemHolidayDefinitionLoader());

            var result = controller.Distance("AUS-NSW", "2014-8-13", "2014-8-10");

            result.Wait();

            Assert.IsNotNull(result);

            Assert.IsNotNull(result.Result as BadRequestErrorMessageResult);
        }

        [TestMethod]
        public async Task AUSNSWInvalidCaseAsync()
        {
            var controller = new HolidayCalculatorController(new FileSystemHolidayDefinitionLoader());

            var result = await controller.Distance("AUS-NSW", "2014-8-13", "2014-8-10");

            Assert.IsNotNull(result);

            Assert.IsNotNull(result as BadRequestErrorMessageResult);
        }

    }
}
