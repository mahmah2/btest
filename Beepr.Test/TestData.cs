﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beepr.Test
{
    public class TestData
    {
        public static readonly string SampleDefinitionsXml = @"<?xml version=""1.0"" encoding=""utf-8"" ?>
                                            <PublicHolidayDefinitions>
  
                                              <WeekendDays>
                                                <Day name = ""Saturday"" />
                                                <Day name=""Sunday"" />
                                              </WeekendDays>

                                              <Definitions>
                                                <FixedDateHoliday>
                                                  <Title>Anzac Day</Title>
                                                  <MonthNumber>4</MonthNumber>
                                                  <DayNumberInMonth>25</DayNumberInMonth>
                                                </FixedDateHoliday>

                                                <FirstWeekdayHoliday>
                                                  <Title>New Year</Title>
                                                  <MonthNumber>1</MonthNumber>
                                                  <DayNumberInMonth>1</DayNumberInMonth>
                                                </FirstWeekdayHoliday>

                                                <NOccurenceHoliday>
                                                  <Title>Queen's Birthday</Title>
                                                  <MonthNumber>6</MonthNumber>
                                                  <Count>2</Count>
                                                  <DayOfTheWeek>Monday</DayOfTheWeek>
                                                </NOccurenceHoliday>
                                              </Definitions>

                                            </PublicHolidayDefinitions>";
    }
}
