﻿using System;
using Beepr.Business;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Beepr.Test
{
    [TestClass]
    public class CalendarTests
    {
        [TestMethod]
        public void AUSNSWGeneralTests()
        {
            var calculator = new PublicHolidayCalculator(new FileSystemHolidayDefinitionLoader() ,
                 new FileSystemLogger(), TestData.SampleDefinitionsXml);

            var startDate = new DateTime(2014, 8, 7);
            var finishDate = new DateTime(2014, 8, 11);

            Assert.AreEqual(1, calculator.ComputeWorkDaysBetween(startDate, finishDate));

            Assert.ThrowsException<ArgumentException>(() => calculator.ComputeWorkDaysBetween(finishDate, startDate));

            startDate = new DateTime(2014, 8, 13);
            finishDate = new DateTime(2014, 8, 21);

            Assert.AreEqual(5, calculator.ComputeWorkDaysBetween(startDate, finishDate));
        }



        [TestMethod]
        public void TestSerialisation()
        {
            var loadedDefinitions = Beepr.Util.Serialization.Deserialize<PublicHolidayDefinitions>(TestData.SampleDefinitionsXml);


            Assert.AreEqual(3, loadedDefinitions.Count);
        }
    }
}
